Create database usermanagement;

CREATE  TABLE IF NOT EXISTS `usermanagement`.`users` (
  `user_Id` INT  auto_increment ,
  `user_Name` VARCHAR(150) NOT NULL ,
  `password` VARCHAR(255) ,
  `postal_Address` VARCHAR(255) ,
  `contact_Number` VARCHAR(10),
  `email` VARCHAR(255) ,
  PRIMARY KEY (`user_Id`) );